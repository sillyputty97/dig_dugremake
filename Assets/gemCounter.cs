﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gemCounter : MonoBehaviour
{
    public static int gemAmount;
    private Text gemCount;
    // Start is called before the first frame update
    void Start()
    {
        gemCount = GetComponent<Text>();
        gemAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        gemCount.text = "Coins: " + gemAmount;
    }
}
