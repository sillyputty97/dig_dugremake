﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameflow : MonoBehaviour
{
    public Transform dirt1Obj;
    public Transform dirt2Obj;
    public Transform dirt3Obj;
    public Transform dirtBGObj;

    public static string supportGone = "n";
    // Start is called before the first frame update
    void Start()
    {
        for (float xPos = -8.25f; xPos < 8.75f; xPos+=1.25f)
        {
            Instantiate(dirt1Obj, new Vector2(xPos, 3), dirt1Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, 3), dirtBGObj.rotation);
            Instantiate(dirt1Obj, new Vector2(xPos, 1.77f), dirt1Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, 1.77f), dirtBGObj.rotation);

            Instantiate(dirt2Obj, new Vector2(xPos, .54f), dirt2Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, .54f), dirtBGObj.rotation);
            Instantiate(dirt2Obj, new Vector2(xPos, -.69f), dirt2Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, -.69f), dirtBGObj.rotation);

            Instantiate(dirt3Obj, new Vector2(xPos, -1.92f), dirt2Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, -1.92f), dirtBGObj.rotation);
            Instantiate(dirt3Obj, new Vector2(xPos, -3.15f), dirt2Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, -3.15f), dirtBGObj.rotation);
            Instantiate(dirt3Obj, new Vector2(xPos, -4.38f), dirt2Obj.rotation);
            Instantiate(dirtBGObj, new Vector2(xPos, -4.38f), dirtBGObj.rotation);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
