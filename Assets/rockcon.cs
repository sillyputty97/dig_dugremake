﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rockcon : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<PolygonCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<PolygonCollider2D>().enabled = true;
        if (gameflow.supportGone == "y")
        {
            StartCoroutine(dropRock());
            gameflow.supportGone = "n";
        }
    }

    

    IEnumerator dropRock()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Rigidbody2D>().gravityScale = 1;
    }
}
