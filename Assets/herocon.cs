﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class herocon : MonoBehaviour

 
{
    public Rigidbody2D rb;
    

    public float xVel = 0;
    public float yVel = 0;
    public static float healthAmount;
    // Start is called before the first frame update
    void Start()
    {
        healthAmount = 1;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            xVel = .5f;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            xVel = -.5f;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            yVel = .5f;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            yVel = -.5f;
        }
        
        if (xVel > 0)
        {
            yVel = 0;
        }

        if (yVel > 0)
        {
            xVel = 0;
        }

        if (xVel < 0)
        {
            yVel = 0;
        }

        if (yVel < 0)
        {
            xVel = 0;
        }


        GetComponent<Rigidbody2D>().velocity = new Vector2(2 * xVel, 2 * yVel);
        if (healthAmount <= 0)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Gem")){
            gemCounter.gemAmount += 1;
            Destroy(collision.gameObject);
         
        }

        if (collision.gameObject.name.Equals("Rock"))
        {
            healthAmount -= 1;
        }
            
    }
}
