﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tilecon : MonoBehaviour
{
    public Vector2 tilePos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tilePos = GetComponent<Transform>().position;

        if ((tilePos.x == -7) && (tilePos.y == 1.77f))
        {
            Destroy(gameObject);
        }

        if ((tilePos.x == -5.75f) && (tilePos.y == 1.77f))
        {
            Destroy(gameObject);
        }

        if ((tilePos.x == 1.75f) && (tilePos.y == .54f))
        {
            Destroy(gameObject);
        }

        if ((tilePos.x == 3) && (tilePos.y == .54f))
        {
            Destroy(gameObject);
        }

        if ((tilePos.x == -.75f) && (tilePos.y == -1.92f))
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "player placeholder")
        {
            if (gameObject.name == "rocksupport")
            {
                gameflow.supportGone = "y";
                Debug.Log("success");
            }
            Destroy(gameObject);
        }
    }
}
